import Vue from 'vue'
import VueRouter from "vue-router"
import Page_1 from "./components/Page_1.vue"
import TestAxios from "./components/TestAxios.vue"

Vue.use(VueRouter)

const routes = [
    { path: '/page1', component: Page_1 },
    { path: '/test_axios', component: TestAxios }
]

const router = new VueRouter({
    routes: routes
})

export default router
