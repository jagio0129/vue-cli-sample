FROM node:14.4-alpine

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install

CMD ["yarn", "serve"]
